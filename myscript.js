// event pada saat link di klik
$('.page-scroll').on('click', function (e) {
    // pada saat klik navbar akan muncul consol ok
    // console.log('ok');

    // ambil isi href misal #about, #portfolio, #contact
    var getHref = $(this).attr('href');
    // console.log(getHref);

    // tangkap elemen yang bersangkutan (ambil sectionnya)
    var elementGetHref = $(getHref);
    // console.log(elementGetHref.offset().top); //ambil element supaya mengetahi jarak element tsb ke paling atas website kita

    // console.log($('body').scrollTop());

    // pindahkan scroll
    $('html, body').animate({
        scrollTop: elementGetHref.offset().top - 50
    }, 1000, 'easeInOutExpo');

    // web untuk download efek animasi easinoutexpo https://gsgd.co.uk/sandbox/jquery/easing/

    // e.preventDefault bertujuan saat di klik hrefnya ga jalan
    // manghasilkan nilai 0. 0 ini jarak body ke bagian paling atas halamannya
    e.preventDefault();


});